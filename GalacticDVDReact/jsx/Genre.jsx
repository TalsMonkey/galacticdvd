﻿var MovieGridRow = React.createClass({

          render: function () {

            var imageUrl = "/Genre/Image?id=" + this.props.item.DvdId;
            var linkUrl = "/Genre/Details?id=" + this.props.item.DvdId;
        


            return (
            <div className="flexcontainer-genre">
            <div className="flex-item-small"><img className="imageGenre" src={imageUrl} /></div>
            <div className="flex-item-big">
              {this.props.item.Description}
              <div><b/><b/><a href={linkUrl}>Details</a></div>
              
            </div>


                    </div>

            );
          }

        });


var MovieGridTable = React.createClass({
  getInitialState: function () {
    return {
      items: []
    }
  },
  componentDidMount: function () {

    $.get(this.props.dataUrl, function (data) {
      if (this.isMounted()) {
        this.setState({
          items: data
        });
      }
    }.bind(this));
  },
  render: function () {

    var rows = [];
    this.state.items.forEach(function (item) {
      rows.push(
            <MovieGridRow key={item.DvdId} item={item} />);
    });
    return (
<div>

  {rows}

</div>);
  }

});

   

       