﻿var EmployeeGridRow = React.createClass({
	render : function(){
		return (
		<tr>
			<td>{this.props.item.Title}</td>
			<td>{this.props.item.Description}</td>
			
		</tr>
		);
	} 
});

var EmployeeGridTable = React.createClass({
	getInitialState: function(){
		return {
			items:[]
		}
	},
	componentDidMount:function(){
	
	$.get(this.props.dataUrl, function(data){
		if(this.isMounted()){
			this.setState({
				items: data
			});
		}
	}.bind(this));
	},
	render : function(){
		var rows = [];
		this.state.items.forEach(function(item){
			rows.push(
			<EmployeeGridRow key={item.DvdId} item={item} />);
});
return (
<table className="table table-bordered table-responsive">
	<thead>
		<tr>
			<th>Title</th>
			<th>Description</th>
			
		</tr>
	</thead>
	<tbody>
		{rows}
	</tbody>
</table>);
}

});
