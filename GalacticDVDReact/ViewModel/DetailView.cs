﻿using GalacticDVDReact.EntityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GalacticDVDReact.ViewModel
{
	//public class DetailId
	//{
	//	public int DvdId { get; set; }
	//}
	public class DetailView
	{


		public int DvdId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Starring { get; set; }


		public Nullable<decimal> Price { get; set; }
		public string Genre { get; set; }
		public byte[] Image { get; set; }
	}
}