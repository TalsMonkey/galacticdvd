﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GalacticDVDReact.EntityModels;

namespace GalacticDVDRentals.ViewModel
{

	public class GenreModel
	{
		public string Genre { get; set; }
	}
	public class DvdInfoModel
	{
		public int DvdId { get; set; }
		public string Title { get; set; }

		public string Description { get; set; }

	//	public byte[] Image { get; set; }
		public string Genre { get; set; }

	}
}