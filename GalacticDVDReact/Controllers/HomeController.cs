﻿using AutoMapper;
using GalacticDVDReact.EntityModels;
using GalacticDVDReact.ViewModel;
using GalacticDVDRentals.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GalacticDVDReact.Controllers
{
    public class HomeController : Controller
    {
        GalacticDBEntities db = new GalacticDBEntities();
        private IMapper _mapper;
        public HomeController()
        {

            _mapper = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DVDInfo, GenreModel>()
                    .ForSourceMember(src => src.Starring, opts => opts.Ignore())
                    .ForSourceMember(src => src.Price, opts => opts.Ignore());



                cfg.CreateMap<DVDInfo, DetailView>();

            }).CreateMapper();
        }


        // GET: Home
        public ActionResult Index()
        {

            GenreTextGet();
            return View(new GenreModel());
        }

        public ActionResult GenreTextGet()
        {

            using (var lookup = new GalacticDBEntities())
            {
                var genreList = lookup
                    .DVDInfoes
                    .Select(a => a.Genre)
                    .Distinct()
                    .OrderBy(s => s)
                    .ToList();

                ViewBag.genreListing = genreList;
                return View();

            }




        }
    }
    
}