﻿using GalacticDVDReact.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using GalacticDVDReact.ViewModel;

using GalacticDVDRentals.ViewModel;
using System.IO;
using System.Drawing;

namespace GalacticDVDReact.Controllers
{
	public class GenreController : Controller
	{
		GalacticDBEntities db = new GalacticDBEntities();
		private IMapper _mapper;

		//Automapper config
		public GenreController()
		{
			_mapper = new AutoMapper.MapperConfiguration(cfg =>
			{
				cfg.CreateMap<DVDInfo, DvdInfoModel>()
					.ForSourceMember(src => src.Starring, opts => opts.Ignore())
					.ForSourceMember(src => src.Price, opts => opts.Ignore());



				cfg.CreateMap<DVDInfo, DetailView>();
				
			}).CreateMapper();
		}


		//Passing JSON to react components in Movie.jsx
		public JsonResult GetDvdInfoForGenre(string genre)
		{
			using (var context = new GalacticDBEntities())
			{
				//Mapper
				var moviesForGenre = context.DVDInfoes.Where(a => a.Genre == genre).ToList();
				var data = _mapper.Map<IEnumerable<DVDInfo>, IEnumerable<DvdInfoModel>>(moviesForGenre);
				
				//JSON result
				return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
			}
		}
		/// </summary>

		//Action View Result
		public ActionResult Index(string genrepass1)
		{
			return View(new  GenreModel{ Genre = genrepass1} );
			
		}

		public JsonResult getDetailInfoForDetails(int newId)
		{

			var test2 = db.DVDInfoes.FirstOrDefault(a => a.DvdId == newId);
			var movieDetail = _mapper.Map<DetailView>(test2);
			return new JsonResult { Data = movieDetail, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

		}

		public ActionResult Details (int id)
		{
			return View(new DetailView { DvdId = id});

		}


		[HttpGet]
		public ActionResult Image(int id)
		{
			using (var context = new GalacticDBEntities())
			{
				var image = context.DVDInfoes.First(info => info.DvdId == id).Image;
				return new FileStreamResult(new MemoryStream(image), "image/png");
			}
		}
		[HttpGet]
		public JsonResult DropDownList()
		{

			using (var lookup = new GalacticDBEntities())
			{
				var genreNames = lookup
					.DVDInfoes
					.Select(a => a.Genre)
					.Distinct()
					.OrderBy(s => s)
					.ToList();

				//List<string> dropdownlist = new List<string>();
				//var count = "";
				//foreach (var item in genreNames)
				//{
				//	if (count != item)
				//	{
				//		count = item;
				//		dropdownlist.Add(count);
				//	}
				//}

				return new JsonResult { Data = genreNames, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

			}




		}

	}




	}
	

