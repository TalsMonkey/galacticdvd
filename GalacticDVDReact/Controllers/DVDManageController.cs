﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GalacticDVDReact.EntityModels;

namespace GalacticDVDReact.Controllers
{
    public class DVDManageController : Controller
    {
        private GalacticDBEntities db = new GalacticDBEntities();

        // GET: DVDManage
        public ActionResult Index()
        {
            return View(db.DVDInfoes.ToList());
        }

        // GET: DVDManage/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DVDInfo dVDInfo = db.DVDInfoes.Find(id);
            if (dVDInfo == null)
            {
                return HttpNotFound();
            }
            return View(dVDInfo);
        }

        // GET: DVDManage/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DVDManage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DVDInfo dVDInfo, HttpPostedFileBase image1)
        {
            DVDInfo b1 = new DVDInfo();
            if (image1 != null)
            {
                dVDInfo.Image = new byte[image1.ContentLength];
                image1.InputStream.Read(dVDInfo.Image, 0, image1.ContentLength);
            }
            if (ModelState.IsValid)
            {
                db.DVDInfoes.Add(dVDInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dVDInfo);
        }

        // GET: DVDManage/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DVDInfo dVDInfo = db.DVDInfoes.Find(id);
            if (dVDInfo == null)
            {
                return HttpNotFound();
            }
            return View(dVDInfo);
        }

        // POST: DVDManage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DvdId,Title,Description,Starring,Price,Genre,Image")] DVDInfo dVDInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dVDInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dVDInfo);
        }

        // GET: DVDManage/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DVDInfo dVDInfo = db.DVDInfoes.Find(id);
            if (dVDInfo == null)
            {
                return HttpNotFound();
            }
            return View(dVDInfo);
        }

        // POST: DVDManage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DVDInfo dVDInfo = db.DVDInfoes.Find(id);
            db.DVDInfoes.Remove(dVDInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
